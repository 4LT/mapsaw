package mapsaw

import (
    "io"
    "bufio"
    "regexp"
    "strconv"
    "unicode"
    "unicode/utf8"
    "strings"
)

const (
    TokOBrace = iota
    TokCBrace
    TokOBracket
    TokCBracket
    TokOParen
    TokCParen
    TokString
    TokNumber
    TokError
    TokEOF
)

const (
    lexNormal = iota
    lexSlash
    lexComment
    lexQuote
    lexEndQuote
    lexWord
)

type Token struct {
    Tag int
    Line uint
    Value string
}

type ParseError struct {
    Line uint
    Message string
}

func wrongToken(tok Token) ParseError {
    if tok.Tag == TokError {
        return ParseError{tok.Line, tok.Value}
    } else if tok.Tag == TokEOF {
        return ParseError{tok.Line, "Unexpected end-of-file"}
    } else {
        if tok.Line < 1 {
            panic("Token has line " +
                strconv.FormatUint(uint64(tok.Line), 10) +"!")
        }
        return ParseError{tok.Line, "Unexpected token '" + tok.Value + "'"}
    }
}

func (err ParseError) Error() string {
    return "Line " + strconv.FormatUint(uint64(err.Line), 10) + ": " +
        err.Message
}

func Lex(input io.Reader, bufSize uint) chan Token {
    tokStream := make(chan Token, bufSize)
    go lex(input, tokStream)
    return tokStream
}

func lex(input io.Reader, tokStream chan<- Token) {
    defer close(tokStream)

    var lineNum uint
    lineNum = 1
    // Number RE: support integers, decimals, exponents
    numberRe := regexp.MustCompile(`^-?\d+\.?\d*([eE]-?\d+)?$`)
    bufReader := bufio.NewReader(input)
    var tokBuilder strings.Builder
    state := lexNormal

    for {
        r, _, err := bufReader.ReadRune()

        if err != nil && err != io.EOF {
            tokStream <- Token{TokError, lineNum, err.Error()}
            break
        }

        if r == utf8.RuneError {
            tokStream <- Token{TokError, lineNum, err.Error()}
            break
        }

        switch state {
        case lexNormal:
            if err == nil && !unicode.IsSpace(r) {
                switch r {
                case '/':
                    tokBuilder.WriteRune(r)
                    state = lexSlash
                case '"':
                    state = lexQuote
                default:
                    tokBuilder.WriteRune(r)
                    state = lexWord
                }
            }
        case lexSlash:
            if err == nil && !unicode.IsSpace(r) {
                if r == '/' {
                    tokBuilder.Reset()
                    state = lexComment
                } else {
                    state = lexWord
                }
            } else {
                tokStream <- Token{TokString, lineNum, tokBuilder.String()}
                tokBuilder.Reset()
                state = lexNormal
            }
        case lexComment:
            if err == nil && r == '\n' {
                state = lexNormal
            }
        case lexQuote:
            if err != nil {
                tokStream <- Token{TokError, lineNum, "End-of-file before " +
                    "closing quote"}
            } else if r == '"' {
                tokStream <- Token{TokString, lineNum, tokBuilder.String()}
                tokBuilder.Reset()
                state = lexEndQuote
            } else {
                tokBuilder.WriteRune(r)
            }
        case lexEndQuote:
            if err == nil && !unicode.IsSpace(r) {
                tokStream <- Token{TokError, lineNum, "Expected space or " +
                    "end-of-file after closing quote"}
                switch r {
                case '/':
                    tokBuilder.WriteRune(r)
                    state = lexSlash
                case '"':
                    state = lexQuote
                default:
                    tokBuilder.WriteRune(r)
                    state = lexWord
                }
            } else {
                state = lexNormal
            }
        // lexWord
        default:
            if err == nil && !unicode.IsSpace(r) {
                tokBuilder.WriteRune(r)
            } else {
                word := tokBuilder.String()
                tokBuilder.Reset()
                switch {
                case word == "{":
                    tokStream <- Token{TokOBrace, lineNum, "{"}
                case word == "}":
                    tokStream <- Token{TokCBrace, lineNum, "}"}
                case word == "[":
                    tokStream <- Token{TokOBracket, lineNum, "["}
                case word == "]":
                    tokStream <- Token{TokCBracket, lineNum, "]"}
                case word == "(":
                    tokStream <- Token{TokOParen, lineNum, "("}
                case word == ")":
                    tokStream <- Token{TokCParen, lineNum, ")"}
                case numberRe.MatchString(word):
                    tokStream <- Token{TokNumber, lineNum, word}
                default:
                    tokStream <- Token{TokString, lineNum, word}
                }
                state = lexNormal
            }
        }

        // EOF
        if err != nil {
            tokStream <- Token{TokEOF, lineNum, "End-of-file"}
            break
        }

        if r == '\n' {
            lineNum++
            if lineNum < 1 {
                panic("Line number overflow!")
            }
        }
    }
}

func Parse(tokStream <-chan Token, warnings io.Writer) (Map, error) {
    lookAhead := <-tokStream
    warnOut := bufio.NewWriter(warnings)
    defer warnOut.Flush()

    var parseMap func() (Map, error)
    var parseEntity func() (Entity, error)
    var parseKVPairs func() (map[string]string, error)
    var parseBrushes func() ([]Brush, error)
    var parseKVPair func() (KVPair, error)
    var parseBrush func() (Brush, error)
    var parseFace func() (Face, error)
    var parsePoint func() (Point, error)
    var parseAxis func() (Axis, error)
    var parseFloats func(n int) ([]float64, error)

    parseMap = func() (Map, error) {
        entities := make([]Entity, 0, 128)

        for lookAhead.Tag != TokEOF {
            entity, err := parseEntity()

            if err == nil {
                entities = append(entities, entity)
            } else {
                return entities, err
            }
        }

        return entities, nil
    }

    parseEntity = func() (Entity, error) {
        if lookAhead.Tag != TokOBrace {
            return Entity{}, wrongToken(lookAhead)
        }

        lookAhead = <-tokStream
        var kvPairs map[string]string
        var brushes []Brush
        var err error

        kvPairs, err = parseKVPairs()

        if err != nil {
            return Entity{}, err
        }

        brushes, err = parseBrushes()

        if err != nil {
            return Entity{}, err
        }

        return Entity{kvPairs, brushes}, nil
    }

    parseKVPairs = func() (map[string]string, error) {
        kvPairs := make(map[string]string, 4)

        for lookAhead.Tag == TokString {
            line := lookAhead.Line
            kv, err := parseKVPair()

            if err == nil {
                _, dup := kvPairs[kv.Key]
                if dup {
                    warnOut.WriteString("Line " +
                        strconv.FormatUint(uint64(line), 10) +
                        ": Duplicate key '" + kv.Key + "'\n")
                }
                kvPairs[kv.Key] = kv.Value
            } else {
                return kvPairs, err
            }
        }

        return kvPairs, nil
    }

    parseBrushes = func() ([]Brush, error) {
        brushes := make([]Brush, 0)

        for lookAhead.Tag != TokCBrace {
            brush, err := parseBrush()

            if err == nil {
                brushes = append(brushes, brush)
            } else {
                return []Brush{}, err
            }
        }

        lookAhead = <-tokStream
        return brushes, nil
    }

    parseKVPair = func() (KVPair, error) {
        if lookAhead.Tag != TokString {
            return KVPair{}, wrongToken(lookAhead)
        }

        key := lookAhead.Value
        lookAhead = <-tokStream

        if lookAhead.Tag != TokString {
            return KVPair{}, wrongToken(lookAhead)
        }

        value := lookAhead.Value
        lookAhead = <-tokStream
        return KVPair{key, value}, nil
    }

    parseBrush = func() (Brush, error) {
        if lookAhead.Tag != TokOBrace {
            return Brush{}, wrongToken(lookAhead)
        }

        lookAhead = <-tokStream
        faces := make([]Face, 0, 6)

        for lookAhead.Tag != TokCBrace {
            face, err := parseFace()

            if err == nil {
                faces = append(faces, face)
            } else {
                return Brush{}, err
            }
        }

        if lookAhead.Tag != TokCBrace {
            return Brush{}, wrongToken(lookAhead)
        }

        lookAhead = <-tokStream
        return faces, nil
    }

    parseFace = func() (Face, error) {
        var face Face
        var err error

        for i := 0; i < 3; i++ {
            face.Plane[i], err = parsePoint()

            if err != nil {
                return face, err
            }
        }

        // Accept anything but error
        if lookAhead.Tag != TokError {
            face.Texture = lookAhead.Value
        } else {
            return face, wrongToken(lookAhead)
        }

        lookAhead = <-tokStream

        if lookAhead.Tag == TokOBracket {
            face.ValveFmt = true
            face.UAxis, err = parseAxis()

            if err != nil {
                return face, err
            }

            face.VAxis, err = parseAxis()

            if err != nil {
                return face, err
            }
        } else {
            face.ValveFmt = false

            if lookAhead.Tag == TokNumber {
                face.XOff, _ = strconv.ParseFloat(lookAhead.Value, 64)
            } else {
                return face, wrongToken(lookAhead)
            }

            lookAhead = <-tokStream

            if lookAhead.Tag == TokNumber {
                face.YOff, _ = strconv.ParseFloat(lookAhead.Value, 64)
            } else {
                return face, wrongToken(lookAhead)
            }

            lookAhead = <-tokStream
        }

        if lookAhead.Tag == TokNumber {
            face.Rot, _ = strconv.ParseFloat(lookAhead.Value, 64)
        } else {
            return face, wrongToken(lookAhead)
        }

        lookAhead = <-tokStream

        if lookAhead.Tag == TokNumber {
            face.XScale, _ = strconv.ParseFloat(lookAhead.Value, 64)
        } else {
            return face, wrongToken(lookAhead)
        }

        lookAhead = <-tokStream

        if lookAhead.Tag == TokNumber {
            face.YScale, _ = strconv.ParseFloat(lookAhead.Value, 64)
        } else {
            return face, wrongToken(lookAhead)
        }

        lookAhead = <-tokStream
        return face, nil
    }

    parsePoint = func() (Point, error) {
        var p Point

        if lookAhead.Tag != TokOParen {
            return p, wrongToken(lookAhead)
        }

        lookAhead = <-tokStream
        v, err := parseFloats(3)

        if err != nil {
            return p, err
        }

        p.X = v[0]
        p.Y = v[1]
        p.Z = v[2]

        if lookAhead.Tag != TokCParen {
            return p, wrongToken(lookAhead)
        }

        lookAhead = <-tokStream
        return p, nil
    }

    parseAxis = func() (Axis, error) {
        var ax Axis

        if lookAhead.Tag != TokOBracket {
            return ax, wrongToken(lookAhead)
        }

        lookAhead = <-tokStream
        v, err := parseFloats(4)

        if err != nil {
            return ax, err
        }

        ax.X = v[0]
        ax.Y = v[1]
        ax.Z = v[2]
        ax.Offset = v[3]

        if lookAhead.Tag != TokCBracket {
            return ax, wrongToken(lookAhead)
        }

        lookAhead = <-tokStream
        return ax, nil
    }

    parseFloats = func(n int) ([]float64, error) {
        v := make([]float64, n)

        for i := 0; i < n; i++ {
            if lookAhead.Tag == TokNumber {
                v[i], _ = strconv.ParseFloat(lookAhead.Value, 64)
            } else {
                return v, wrongToken(lookAhead)
            }

            lookAhead = <-tokStream
        }

        return v, nil
    }

    return parseMap()
}
