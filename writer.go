package mapsaw

import (
    "io"
    "bufio"
    "fmt"
    "strconv"
)

func Write(m *Map, output io.Writer, warnings io.Writer) error {
    var err error
    indentLevel := 0
    out := bufio.NewWriter(output)
    outWarn := bufio.NewWriter(warnings)
    // Solid and entity counter
    vmfObjIdx := 1
    // Global face counter
    vmfSideIdx := 1

    var writeMap func(m *Map) error
    var writeEntity func(ent *Entity, mapEntIdx int) error
    var writeKeyValues func(keyValues map[string]string, mapEntIdx int) error
    var writeBrushes func(brushes []Brush, mapEntIdx int) error
    var writeFace func(face *Face, mapEntIdx int, mapBrushIdx int,
        mapFaceIdx int) error

    indent := func(line string) string {
        pfx := ""
        for i := 0; i < indentLevel; i++ {
            pfx = pfx + "    "
        }
        return pfx + line
    }

    quote := func(s string) string {
        return "\"" + s + "\""
    }

    kvString := func(key string, value string) string {
        return quote(key) + " " + quote(value)
    }

    floatString := func(f float64) string {
        return strconv.FormatFloat(f, 'f', -1, 64)
    }

    planeString := func(plane *[3]Point) string {
        retString := ""
        for i := 0; i < 3; i++ {
            retString+= "(" + floatString((*plane)[i].X) + " " +
                floatString((*plane)[i].Y) + " " + floatString((*plane)[i].Z) +
                ")"
            if i <= 1 {
                retString+= " "
            }
        }
        return retString
    }

    axisString := func(axis *Axis, scale float64) string {
        return "[" + floatString(axis.X) + " " + floatString(axis.Y) +
            " " + floatString(axis.Z) + " " + floatString(axis.Offset) +
            "] " + floatString(scale)
    }

    writeMap = func(m *Map) error {
        mapEntIdx := 0
        for _, ent := range *m {
            err = writeEntity(&ent, mapEntIdx)
            if err != nil {
                return err
            }
            mapEntIdx++
        }
        return nil
    }

    writeEntity = func(ent *Entity, mapEntIdx int) error {
        class, ok := ent.KeyValues["classname"]
        var err error

        if ok {
            if class == "worldspawn" {
                _, err = fmt.Fprintln(out, "world\n{")
            } else {
                _, err = fmt.Fprintln(out, "entity\n{")
            }
        } else {
            _, err = fmt.Fprintf(outWarn, "Entity %v: No class name\n",
                mapEntIdx)
        }

        if err != nil {
            return err
        }

        indentLevel++
        err = writeKeyValues(ent.KeyValues, mapEntIdx)
        if err != nil {
            return err
        }

        err = writeBrushes(ent.Brushes, mapEntIdx)
        if err != nil {
            return err
        }
        indentLevel--

        _, err = fmt.Fprintln(out, "}")
        return err
    }

    writeKeyValues = func(keyValues map[string]string, mapEntIdx int) error {
        _, err = fmt.Fprintln(out,
            indent(kvString("id", strconv.Itoa(vmfObjIdx))))
        vmfObjIdx++
        if err != nil {
            return err
        }

        for key, value := range keyValues {
            if key == "id" {
                _, err = fmt.Fprintf(outWarn, "Entity %v: Ignoring 'id' key",
                    mapEntIdx)
            } else {
                _, err = fmt.Fprintln(out, indent(kvString(key, value)))
            }

            if err != nil {
                return err
            }
        }
        return nil
    }

    writeBrushes = func(brushes []Brush, mapEntIdx int) error {
        mapBrushIdx := 0
        for _, b := range brushes {
            _, err = fmt.Fprintln(out, indent("solid\n") + indent("{"))
            if err != nil { return err }

            indentLevel++
            _, err = fmt.Fprintln(out, indent(kvString("id",
                strconv.Itoa(vmfObjIdx))))
            vmfObjIdx++
            if err != nil { return err }

            for _, face := range b {
                mapFaceIdx := 0

                err = writeFace(&face, mapEntIdx, mapBrushIdx, mapFaceIdx)
                if err != nil { return err }

                mapFaceIdx++
            }
            indentLevel--

            _, err = fmt.Fprintln(out, indent("}"))
            if err != nil { return err }

            mapBrushIdx++
        }
        return nil
    }

    writeFace = func(face *Face, mapEntIdx int, mapBrushIdx int,
            mapFaceIdx int) error {
        if !face.ValveFmt {
            err = face.ConvertToValveFormat(mapEntIdx, mapBrushIdx, mapFaceIdx)
        }

        skipAxes := false
        if err != nil {
            // invalid texture axis -> suppress error and skip axes
            _, err = fmt.Fprintf(outWarn, "Entity %v: Brush %v: Face %v: " +
                "Bad plane, skipping texture axes\n", mapEntIdx, mapBrushIdx,
                mapFaceIdx)
            skipAxes = true
        }

        if err != nil { return err }
        _, err = fmt.Fprintln(out, indent("side\n") + indent("{"))
        if err != nil { return err }
        indentLevel++

        _, err = fmt.Fprintln(out, indent(
            kvString("id", strconv.Itoa(vmfSideIdx))))
        vmfSideIdx++
        if err != nil { return err }

        _, err = fmt.Fprintln(out, indent(
            kvString("plane", planeString(&face.Plane))))
        if err != nil { return err }

        _, err = fmt.Fprintln(out, indent(kvString("material", face.Texture)))
        if err != nil { return err }

        if !skipAxes {
            _, err = fmt.Fprintln(out, indent(kvString("uaxis",
                axisString(&face.UAxis, face.XScale))))
            if err != nil { return err }

            _, err = fmt.Fprintln(out, indent(kvString("vaxis",
                axisString(&face.VAxis, face.YScale))))
            if err != nil { return err }
        }

        _, err = fmt.Fprintln(out, indent(kvString("rotation",
                strconv.FormatFloat(face.Rot, 'f', -1, 64))))
        if err != nil { return err }

        _, err = fmt.Fprintln(out, indent(kvString("lightmapscale", "16")))
        if err != nil { return err }

        _, err = fmt.Fprintln(out, indent(kvString("smoothing_groups", "0")))
        if err != nil { return err }

        indentLevel--
        _, err = fmt.Fprintln(out, indent("}"))
        return err
    }

    err = writeMap(m)
    if err != nil { return err }

    err = out.Flush()
    if err != nil { return err }
    return outWarn.Flush()
}

