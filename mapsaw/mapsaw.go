package main

import (
    "os"
    "fmt"
    "gitlab.com/4LT/mapsaw"
)

func main() {
    fileName := os.Args[1]
    mapFile, err := os.Open(fileName)

    if err != nil {
        fmt.Fprintln(os.Stderr, err)
        os.Exit(1)
    }

    fileNameProper := fileName[:len(fileName) - 4]
    vmfFile, err := os.OpenFile(fileNameProper + ".vmf",
        os.O_CREATE|os.O_WRONLY, 0755)
    if err != nil {
        fmt.Fprintln(os.Stderr, err)
        os.Exit(1)
    }

    ch := mapsaw.Lex(mapFile, 100)
    m, err := mapsaw.Parse(ch, os.Stderr)
    if err != nil {
        fmt.Fprintln(os.Stderr, err)
        os.Exit(1)
    }

    mapsaw.Write(&m, vmfFile, os.Stderr)
}

