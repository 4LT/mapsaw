package mapsaw

import (
    "math"
    "strconv"
)

type (
    Map = []Entity

    Entity struct {
        KeyValues map[string]string
        Brushes []Brush
    }

    KVPair struct {
        Key string
        Value string
    }

    Brush = []Face

    Face struct {
        Plane [3]Point
        Texture string
        XOff float64
        YOff float64
        UAxis Axis
        VAxis Axis
        Rot float64
        XScale float64
        YScale float64
        ValveFmt bool
    }

    Point struct {
        X float64
        Y float64
        Z float64
    }

    Axis struct {
        X float64
        Y float64
        Z float64
        Offset float64
    }

    MapError struct {
        Ent int
        Brush int
        Face int
        msg string
    }
)

const (
    xAxis = iota
    yAxis
    zAxis
)

func (err MapError) Error() string {
    return "Entity " + strconv.Itoa(err.Ent) + " Brush " +
        strconv.Itoa(err.Brush) + " Face " + strconv.Itoa(err.Face) + ": " +
        err.msg
}

func cross(u [3]float64, v [3]float64) [3]float64 {
    var out [3]float64
    out[0] = u[1]*v[2] - u[2]*v[1]
    out[1] = u[2]*v[0] - u[0]*v[2]
    out[2] = u[0]*v[1] - u[1]*v[0]
    return out
}

func normalize(u [3]float64) ([3]float64, bool) {
    mag := math.Sqrt(u[0]*u[0] + u[1]*u[1] + u[2]*u[2])
    if mag == 0.0 {
        return [3]float64{0, 0, 0}, false
    }
    return [3]float64{u[0]/mag, u[1]/mag, u[2]/mag}, true
}

func disp(to Point, from Point) [3]float64 {
    return [3]float64{to.X - from.X, to.Y - from.Y, to.Z - from.Z}
}

func majorAxis(u [3]float64) int {
    magX := math.Abs(u[0])
    magY := math.Abs(u[1])
    magZ := math.Abs(u[2])

    if magX > magY && magX > magZ {
        return xAxis
    } else if magY > magZ {
        return yAxis
    } else {
        return zAxis
    }
}

func (f *Face) ConvertToValveFormat(eIdx int, bIdx int, fIdx int) error {
    if f.ValveFmt {
        return MapError{eIdx, bIdx, fIdx, "Already in Valve format"}
    }

    var s, t [3]float64
    var success bool
    s, success = normalize(disp(f.Plane[2], f.Plane[0]))

    if !success  {
        return MapError{eIdx, bIdx, fIdx, "Bad plane"}
    }

    t, success = normalize(disp(f.Plane[1], f.Plane[0]))

    if !success {
        return MapError{eIdx, bIdx, fIdx, "Bad plane"}
    }

    normal := cross(s, t)
    normAxis := majorAxis(normal)

    if  normAxis == xAxis {
        f.UAxis = Axis{0, 1, 0, f.XOff}
        f.VAxis = Axis{0, 0, 1, f.YOff}
    } else if normAxis == yAxis {
        f.UAxis = Axis{1, 0, 0, f.XOff}
        f.VAxis = Axis{0, 0, 1, f.YOff}
    } else {
        f.UAxis = Axis{1, 0, 0, f.XOff}
        f.VAxis = Axis{0, 1, 0, f.YOff}
    }

    f.ValveFmt = true
    return nil
}
